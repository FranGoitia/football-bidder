"""
Gets current matches from betting houses
"""
import argparse
from datetime import date, timedelta

from bs4 import BeautifulSoup
import requests

from utils.miljugadas import MONTHS

class Match(object):

    def __init__(self, home_team, away_team, date, prizes):
        self.home_team = home_team
        self.away_team = away_team
        self.date = date
        self.prizes = prizes

    def __repr__(self):
        s = "{home: %s, away: %s, prizes: %s }" % (self.home_team, self.away_team, self.prizes)
        return s

    def __str__(self):
        s = "{home: %s, away: %s, prizes: %s }" % (self.home_team, self.away_team, self.prizes)
        return s


def miljugadas(leagues='all'):
    """
    gets betting (matches) available from leagues for the next 7 days
    """
    if 'all':
        # bbva, seriea, premier, ligue1, bundesliga
        league_codes = [23170,23192,48555,23161,23169]
    url = 'https://www.miljugadas.com/es-ES/sportsbook/eventpaths/multi/'
    req = requests.get(url + str(league_codes))
    print(req.url)
    soup = BeautifulSoup(req.text)
    leagues_matches = soup.find('div', {'class': 'rollup market_type market_type_id_1 win_draw_win multi_event game_type'}
              ).find_all('div', {'class': 'rollup event_date'})
    available_matches = []
    for league_matches in leagues_matches:
        # only consider matches that take place in the next week
        matches_day, matches_month = league_matches.find('h2', {'class': 'event_date-title'}).contents[0].split(' ')[1:]
        today = date.today()
        match_date = date(today.year, MONTHS[matches_month], int(matches_day))
        if match_date - today <= timedelta(7):
            matches = league_matches.find_all('tr', {'class': 'event'})
            for match in matches:
                home, draw, away = match.find_all('a')
                home_team = home.span['title']
                home_win = float(home['data-price-decimal'])
                draw = float(draw['data-price-decimal'])
                away_team = away.span['title']
                away_win = float(away['data-price-decimal'])
                prizes = {'home': home_win, 'draw': draw, 'away': away_win}
                match = Match(home_team, away_team, match_date, prizes)
                available_matches.append(match)
    return available_matches


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--betting_house', default='miljugadas')
    parser.add_argument('--leagues', default='all')
    args = parser.parse_args()

    if args.betting_house == 'miljugadas':
        matches = miljugadas(args.leagues)
    print(matches)
