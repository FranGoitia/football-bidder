import sqlite3 as sqlite
import sys
from operator import methodcaller

from matchescrawler import miljugadas


def connect_to_database():
    con = sqlite.connect('football.db')
    cur = con.cursor()
    return cur


class MatchBidder(object):

    def __init__(self, match, cursor):
        self.match = match
        self.cursor = cursor
        self.favorite = None
        self.points = 0

    def _gen_team_form(self, team_id, results):
        """
        generates form measured in points per game metric from last 5 matches.
        match = [home_team, away_team, home_score, away_score]
        """
        if len(results) == 0:
            return None
        points = 0
        for result in results:
            if result[0] == team_id:
                home_score = result[2]
                away_score = result[3]
            else:
                home_score = result[3]
                away_score = result[2]
            if home_score > away_score:
                points += 3
            elif home_score == away_score:
                points += 1
        if points == 0:
            points = 0.00001
        return points / 5

    def home_form(self):
        """
        generates form of last five matches for home team. returns point per game metric
        """
        try:
            id_ = self.cursor.execute("""SELECT id FROM teams WHERE title LIKE :title and country_id IN (110, 112, 117, 225, 114)""",
                              {'title': '%' + self.match.home_team + '%'}).fetchone()[0]
        except:
            return None
        results = self.cursor.execute("""SELECT team1_id, team2_id, score1, score2 FROM games WHERE (team1_id = :id OR team2_id = :id)
                              AND (score1 not NULL) ORDER BY play_AT DESC LIMIT 5""", {'id': id_}).fetchall()
        return self._gen_team_form(id_, results)


    def away_form(self):
        """
        generates form of last five matches for away team. returns points per game metric
        """
        try:
            id_ = self.cursor.execute("""SELECT id FROM teams WHERE title LIKE :title and country_id IN (110, 112, 117, 225, 114)""",
                              {'title': '%' + self.match.away_team + '%'}).fetchone()[0]
        except:
            # name unrecognizable
            return None
        # home_team, away_team, home_score, away_score
        results = self.cursor.execute("""SELECT team1_id, team2_id, score1, score2 FROM games WHERE (team1_id = :id OR team2_id = :id)
                              AND (score1 not NULL) ORDER BY play_AT DESC LIMIT 5""", {'id': id_}).fetchall()
        return self._gen_team_form(id_, results)

    def rate_match(self):
        """
        rates match according to form algorithm
        """
        self.home_form = home_form = self.home_form()
        self.away_form = away_form = self.away_form()
        if home_form is None or away_form is None:
            return 0
        if home_form > away_form:
            better_form = home_form
            worse_form = away_form
            self.favorite = 'home'
        else:
            better_form = away_form
            worse_form = home_form
            self.favorite = 'away'
        self.points = better_form / worse_form * self.match.prizes[self.favorite]
        return self.points

    def __repr__(self):
        s = "{home: [%s, %s], away: [%s, %s], prizes: %s, favorite: %s, points: %s }" % (self.match.home_team,
                self.home_form, self.match.away_team, self.away_form, self.match.prizes, self.favorite, self.points)
        return s

    def __str__(self):
        s = "{home: [%s, %s], away: [%s, %s], prizes: %s, favorite: %s, points: %s }" % (self.match.home_team,
                self.home_form, self.match.away_team, self.away_form, self.match.prizes, self.favorite, self.points)
        return s


if __name__ == '__main__':
    matches = miljugadas('all')
    cursor = connect_to_database()
    matches = [MatchBidder(match, cursor) for match in matches]
    matches = sorted(matches, key=methodcaller('rate_match'), reverse=True)
    print(matches)


# {home: Arsenal, away: Bournemouth, prizes: {'home': 1.4, 'draw': 4.75, 'away': 7.5}, favorite: away, points: 5999999.999999999 }
# {home: Liverpool, away: Leicester, prizes: {'home': 1.97, 'draw': 3.6, 'away': 3.6}, favorite: away, points: 4680000.0 }
