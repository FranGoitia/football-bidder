#!/usr/bin/python
# -*- coding: utf-8

import selenium
from selenium import webdriver
from collections import defaultdict, OrderedDict

MONTHS = {
           'Enero': 1,
           'Febrero': 2,
           'Marzo': 3,
           'Abril': 4,
           'Mayo': 5,
           'Junio': 6,
           'Julio': 7,
           'Agosto': 8,
           'Septiembre': 9,
           'Octubre': 10,
           'Noviembre': 11,
           'Diciembre': 12
         }

LEAGUES = ['Internacional', 'Inglaterra', 'España', 'Francia', 'Alemania', 'Italia', 'Holanda', 'Portugal', 'Australia',
           'Bélgica', 'Egipto', 'Grecia', 'Omán', 'Irán', 'Japón', 'Kuwait', 'Marruecos', 'Arabia Saudí', 'Escocia', 'Turquía',
           'Irlanda del Norte', 'Dinamarca', 'Rusia', 'Emiratos Árabes', 'Gales', 'Túnez', 'Noruega', 'Suecia', 'Argelia', 'Israel']


def codes_generator():
    """
    generates dictionary containing codes for every division available
    """
    codes = defaultdict(dict)
    driver = selenium.webdriver.Chrome(executable_path='/media/Data.II/Dropbox/Projects/football-bidder/utils/chromedriver')
    driver.get('https://www.miljugadas.com/es-ES/sportsbook')
    driver.find_element_by_class_name('sport_240').click()
    for league in LEAGUES:
        try:
            league = driver.find_element_by_link_text(league)
            league.click()
        except selenium.common.exceptions.NoSuchElementException as e:
            continue
        divisions = league.find_element_by_xpath("parent::*").find_elements_by_tag_name('li')
        for division in divisions:
            division = division.find_element_by_tag_name('a')
            division_code = division.get_attribute('data-id')
            division_name = division.text
            codes[league.text][division_name] = division_code
    ordered_codes = OrderedDict({})
    for league in sorted(codes.keys()):
        ordered_codes[league] = codes[league]
    return ordered_codes


if __name__ == '__main__':
    codes = codes_generator()
    print(codes)


"""
[23170, 23192, 48555, 23161, 23169]
OrderedDict([('Alemania', {'Alemania - Bundesliga': '23161', 'Alemania - Copa': '54196', 'Alemania - Bundesliga 3': '24587', 'Alemania - Bundesliga 2': '23795'}),
('Arabia Saudí', {'Arabia Saudí - Premiere': '73094'}), ('Argelia', {'ALG Liga Sub-21': '264271', 'Algeria - División 1': '32637', 'Algeria - División 2': '32980'}),
('Australia', {'Australia - A-League': '81150'}), ('Bélgica', {'Bélgica - Jupiler League': '52995'}), ('Dinamarca', {'Dinamarca - Superliga': '23205'}),
('Egipto', {'Egipto-Segunda División': '113342', 'Premier': '29773'}), ('Emiratos Árabes', {'Emiratos Árabes Unidos- División 1': '115873', 'UAE - League': '94383'}),
('Escocia', {'Escocia - League Two': '56383', 'Escocia - League One': '56382', 'Escocia - Championship': '56279', 'Escocia - Premier': '23155'}),
('España', {'España - Liga BBVA': '23170', 'España - Copa del Rey': '67954'}), ('Francia', {'Francia - Ligue 1': '48555'}), ('Gales', {'Gales - Premier League': '59736'}),
('Grecia', {'Grecia - Super Liga': '53509'}), ('Holanda', {'Holanda - Eredivisie': '47282'}),('Inglaterra', {'Inglaterra - FA Cup': '24159',
'Inglaterra - Northern Premier League': '63193', 'Inglaterra - Copa de la Liga': '53131', 'Conference North': '59948',
'Inglaterra - League Two': '37890', 'Inglaterra - Ryman Premier División': '63199', 'Inglaterra - Premier League': '23169', 'Conference South': '59949',
'Inglaterra - Southern Premier League': '63186', 'Inglaterra - League One': '37881', 'Inglaterra - Championship': '37873', 'Conference Premiership': '23659'}),
('Internacional', {'Mundial - Clasificación - Sudamérica': '87452', 'Europa League': '42127', 'Copa de Federacion Asia de Sur': '119007', 'Mundial 2018': '188999',
'Liga de Campeones': '27911', 'Euro 2016': '32760'}), ('Irlanda del Norte', {'Irlanda del Norte - Premier': '57274'}), ('Irán', {'Iran - Premier League': '144314'}),
('Israel', {'Israel Juvenil': '153114', 'ISR Liga Bet': '278819', 'Israel - Premier League': '62888', 'Israel - Liga Leumit': '62887', 'Israel – Liga Alef': '127428'}),
('Italia', {'Italia - Copa': '50576', 'Italia - Serie B': '24663', 'Italia - Serie A': '23192'}), ('Japón', {'Japón Emperor Cup': '88438'}), ('Kuwait', {'Kuwait – Liga': '128783'}),
('Marruecos', {'Marruecos - Super Liga': '63218'}), ('Noruega', {'Noruega - Tippeligaen': '27295'}), ('Omán', {'Oman - Premier League': '128204'}),
('Portugal', {'Portugal - Liga Zon Sagres': '24246'}), ('Rusia', {'Rusia - Premier League': '31721'}), ('Suecia', {'Suecia - Allsvenskan': '27313'}),
('Turquía', {'Turquía - Primera Liga': '72290', 'Turquía - Copa': '120588', 'Turquía - Superliga': '66493'})])
"""
